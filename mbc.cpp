#include <algorithm>
#include <array>
#include <chrono>
#include <compare> // spaceship
#include <concepts> // same_as
#include <cstddef> // nullptr_t
#include <cstdlib> // EXIT_SUCCESS
#include <deque>
#include <exception>
#include <filesystem>
#include <format>
#include <fstream>
#include <iostream>
#include <iterator> // cbegin
#include <limits>   // numeric_limits
#include <list>
#include <map>
#include <optional>
#include <random>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

/*
  TODO:
  - get rid of duplicated mapping-int-to-basicType functionality if possible
*/

/**
  @file
  The inspiration for this came from a riddle posed to Bjarne Stroustrup:
  assume you are given n numbers one by one, that you are to store in a sorted sequence,
  and later remove from that sequence in a random order.
  For which n will std::list be better than vector?
*/

using std::begin;
using std::cbegin;
using std::cend;
using std::empty;
using std::end;

using std::cerr;

using std::format;
using std::for_each;
using std::get;
using std::hash;
using std::map;
using std::optional;
using std::ostream;
using std::ostringstream;
using std::same_as;
using std::set;
using std::to_string;
using std::tuple;
using std::vector;

using String = std::string; ///< intended as string type that could be changed to char_vec
using StringView = std::string_view;

using SChar = signed char;
using UChar = unsigned char;
using SShort = signed short;
using UShort = unsigned short;
using SInt = signed int;
using UInt = unsigned int;
using SLong = signed long;
using ULong = unsigned long;
using LongLong = long long;
using SLongLong = signed long long;
using ULongLong = unsigned long long;

using CharPTuple = tuple<char const *, char const *>;
using StringSeq = vector<String>;
using StringViewSeq = vector<StringView>;

template <class Int> using EnumNameMap = std::unordered_map<Int, char const *>;
template <class Dest> using StringAnd = tuple<Dest, String>;

using namespace std::literals;



[[noreturn]] void throwOutOfRange(String const &v) {
  throw std::out_of_range(v);
}


[[noreturn]] void throwRuntimeError(String const &v) {
  throw std::runtime_error(v);
}



// from Bjarne Stroustrup's "The C++ Programming Language", 4th ed, ch 11.5
template <class Dest, class Src> auto narrowCast(Src v) {
  auto const r = static_cast<Dest>(v);

  if (static_cast<Src>(r) != v)
    throwOutOfRange(String(__func__));

  return r;
}



// unsigned
template <class Ch, class RndEng> auto myRandint(Ch from, Ch thru, RndEng &randEng)
requires (same_as<Ch,char> && std::is_unsigned<Ch>::value)     ||  same_as<Ch,UChar>
      || (same_as<Ch,wchar_t> && std::is_unsigned<Ch>::value)  ||  same_as<Ch,char16_t>  ||  same_as<Ch,char32_t>
{
  std::uniform_int_distribution<unsigned int> distrib(from, thru);
  return narrowCast<Ch>(distrib(randEng));
}


// signed
template <class Ch, class RndEng> auto myRandint(Ch from, Ch thru, RndEng &randEng)
requires (same_as<Ch,char> && std::is_signed<Ch>::value)    ||  same_as<Ch,SChar>
      || (same_as<Ch,wchar_t> && std::is_signed<Ch>::value)
{
  std::uniform_int_distribution<> distrib(from, thru);
  return narrowCast<Ch>(distrib(randEng));
}


template<class Int, class RndEng> auto myRandint(Int from, Int thru, RndEng &randEng)
requires same_as<Int,short>  || same_as<Int,int>  || same_as<Int,long>  || same_as<Int,LongLong>
      || same_as<Int,UShort> || same_as<Int,UInt> || same_as<Int,ULong> || same_as<Int,ULongLong>
{
  std::uniform_int_distribution<Int> distrib(from, thru);
  return distrib(randEng);
}



// stackoverflow.com/questions/3844909/sorting-a-setstring-on-the-basis-of-length (modified)
template <typename T> struct GtSizeLtLexico final {

  bool operator()(T const &a, T const &b) const {
    auto const sizeA = a.size();
    auto const sizeB = b.size();

    if (sizeA == sizeB)
      return a < b;

    return sizeA > sizeB;
  }

};

namespace MT {

auto static inline calcNow() {
  return std::chrono::high_resolution_clock::now();
}

using TimePoint = decltype(calcNow());

auto static inline timeInMs(TimePoint const &start, TimePoint const &end) {
  return 1000.000L * std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
}

} // namespace MT



template <class T> auto tryConvertAs(T, String const &src, String *reason = nullptr) {
  optional<T> res;
  try {
    if constexpr(same_as<T, long double>)
      res = std::stold(src);
    if constexpr(same_as<T, double>)
      res = std::stod(src);
    if constexpr(same_as<T, float>)
      res = std::stof(src);
    if constexpr(same_as<T, unsigned long long>)
      res = std::stoull(src);
    if constexpr(same_as<T, long long> || same_as<T, signed long long>)
      res = std::stoll(src);
    if constexpr(same_as<T, unsigned long>)
      res = std::stoul(src);
    if constexpr(same_as<T, long> || same_as<T, signed long>)
      res = std::stol(src);
    if constexpr( same_as<T, int> || same_as<T, signed int>)
      res = std::stoi(src);
  } catch (const std::invalid_argument &ex) {
    if (reason != nullptr) {
      (*reason) = ex.what();
    }
  } catch (const std::out_of_range &ex) {
    if (reason != nullptr) {
      (*reason) = ex.what();
    }
  }

  return res;
}



void withPrecision(ostream &o, std::floating_point auto v, std::streamsize precNext) {
  auto const flagsBefore = o.flags();
  auto const precBefore = o.precision();

  o << std::fixed;
  o.precision(precNext);
  o << v;

  o.precision(precBefore);
  o.flags(flagsBefore);
}



[[nodiscard]] static auto const &randomWords() {
  // TODO: add more 'random' words sometime
  static const std::array yarns{
    "cute", "kittens", "puppies", "love", "hate", "deep", "dark", "rotary", "impellers"
  };

  // for now, pretend inserting strings into an unordered set will randomize their order
  static const std::unordered_set<String> unordered(cbegin(yarns), cend(yarns));

  static const StringSeq result(cbegin(unordered), cend(unordered));
  return result;
}


static auto randomSentence() {
  auto const seqRandomWords = randomWords();

  if (seqRandomWords.size() <= 1U)
    throwRuntimeError(String(__func__) + " randomWords() result too small");

  std::random_device rd;
  std::mt19937_64 re(rd());

  auto const seqWordsSize = seqRandomWords.size();
  auto const zero = decltype(seqWordsSize){};
  auto const desiredNumberOfWords = myRandint(zero + 1, seqWordsSize, re);
  auto const chSpace = ' ';

  String result;

  for (auto i = 0U; i < desiredNumberOfWords; ++i) {
    if (!empty(result)) {
      result.push_back(chSpace);
    }

    auto wordIndex = myRandint(zero, seqWordsSize - 1U, re);
    result.append(seqRandomWords.at(wordIndex));
  }

  return result;
}


template <class T> static auto basicToStr(T const &v) {
  ostringstream oss;
  if constexpr(std::is_integral<T>())
    oss << (+v);
  else
    oss << v;
  return oss.str();
}



template <class Int> static auto unexpectedValue(Int const v) {
  return "unexpected value (" + to_string(std::to_underlying(v)) + ')';
}



template <class TMap, class Int> static auto toCharPImp(TMap const &src, Int const v) {
  optional<const char *> res{};
  auto const fr = src.find(v);

  if (fr != end(src)) {
    res = fr->second;
  }

  return res;
}



enum class Verbosity { ///< verbosity levels
  None,
  Verbose,
  Debug
};


static auto verbosityMap() {
  using enum Verbosity;
  EnumNameMap<Verbosity> result{
      {None, "none"},
      {Verbose, "verbose"},
      {Debug, "debug"},
  };
  return result;
}


static auto toCharP(Verbosity const v) {
  return toCharPImp(verbosityMap(), v);
}



template<typename T> concept HasIterator = requires {
  typename T::iterator;
};

template<typename T> concept HasKeyType = HasIterator<T> && requires {
  typename T::key_type;
};


struct OutputVerbosity {
  ostream    *_pOut;
  Verbosity   _verbosity;

  bool writeDebug() const {
    return (_pOut != nullptr) && (Verbosity::Debug == _verbosity);
  }

protected:
  OutputVerbosity(ostream *pOut, Verbosity const verbLev)
  : _pOut(pOut), _verbosity(verbLev) {}
};


template<HasIterator C> class InsertIntoSeq final : public OutputVerbosity {
  C     *_pDest;
  bool   _emplace;

public:

InsertIntoSeq(C *pDest, bool const emplace, Verbosity const verbLev, ostream *pOut)
: OutputVerbosity(pOut, verbLev),
  _pDest(pDest), _emplace(emplace)
{}

template <class T> void operator()(T const &toInsert) {

  if (writeDebug()) {
    (*_pOut) << "\nDBG: *before* elem-add, target = {";

    auto const first = cbegin(*_pDest);
    for(auto cit = first; cit != cend(*_pDest); ++cit){
      (*_pOut) << ( (first == cit) ? "" : ",") << " " << basicToStr(*cit);
    }

    (*_pOut) <<  " }\n";
  }

  typename C::value_type toInsertCpy(toInsert);

  typename C::iterator it;

  if constexpr(HasKeyType<C>) {
    it = _pDest->find(toInsertCpy);
  } else {
    it = std::find_if(
      begin(*_pDest), end(*_pDest), [&toInsertCpy](decltype(toInsertCpy) elem) { return (elem < toInsertCpy) ? false : true; });
  }

  if (end(*_pDest) == it) {
    if (writeDebug())
      (*_pOut) << "DBG: failed to find " << (+toInsert) << " in target\n";

    if constexpr(HasKeyType<C>) {
      // assoc-seq-thing
      auto insertRes = _emplace ? _pDest->emplace(std::move(toInsertCpy)) :
#if defined(USE_MOVE)
                                  _pDest->insert(std::move(toInsertCpy))
#else
                                  _pDest->insert(toInsertCpy)
#endif
      ;
      if (!insertRes.second) {
        if (writeDebug())
          (*_pOut) << "DBG: failed to insert " << (+toInsert) << " into target\n";
      }

    } else {
      // sorted-seq-thing
      if (_emplace) {
        _pDest->emplace_back(std::move(toInsertCpy));
      } else {
  #if defined(USE_MOVE)
        _pDest->push_back(std::move(toInsertCpy));
  #else
        _pDest->push_back(toInsertCpy);
  #endif
      }
    }

  } else {
    if (toInsertCpy == *it) {
      if (writeDebug())
        (*_pOut) << "DBG: logic_error: " << (+toInsert) << " is already present in target!\n";
    } else {
      if (writeDebug())
        (*_pOut) << "DBG: *did* find value > " << (+toInsert) << " in target\n";
    }
  }

  if (writeDebug()) {
    (*_pOut) << "DBG: *after*  elem-add, target = {";

    auto const first = cbegin(*_pDest);
    for(auto cit = first; cit != cend(*_pDest); ++cit){
      (*_pOut) << ( (first == cit) ? "" : ",") << " " << basicToStr(*cit);
    }

    (*_pOut) <<  " }\n";
  }
}
};


template<HasIterator C> class RemoveFromSeq final : public OutputVerbosity {
  C  *_pDest;

public:

RemoveFromSeq(C *dest, Verbosity const verbLev, ostream *out)
: OutputVerbosity(out, verbLev), _pDest(dest)
{}


template <class T> void operator()(T const &toRemove) {

  if (writeDebug()) {
    (*_pOut) << "\nDBG: *before* elem-remove, target = {";

    auto const first = cbegin(*_pDest);
    for(auto cit = first; cit != cend(*_pDest); ++cit){
      (*_pOut) << ( (first == cit) ? "" : ",") << " " << basicToStr(*cit);
    }

    (*_pOut) <<  " }\n";
  }

  typename C::value_type toRemoveCpy(toRemove);

  typename C::iterator it;

  if constexpr(HasKeyType<C>) {
    it = _pDest->find(toRemoveCpy);
  } else {
    it = std::find(begin(*_pDest), end(*_pDest), toRemoveCpy);
  }

  if (it != end(*_pDest)) {
    _pDest->erase(it);
  }

  if (writeDebug()) {
    (*_pOut) << "DBG: *after*  elem-remove, target = {";

    auto const first = cbegin(*_pDest);
    for(auto cit = first; cit != cend(*_pDest); ++cit){
      (*_pOut) << ( (first == cit) ? "" : ",") << " " << basicToStr(*cit);
    }

    (*_pOut) <<  " }\n";
  }
}

};


namespace TMP {

template <class Src, class Dest> void fillAndEmptySeq(
  StringAnd<Dest> &destEtc, Src const &src, bool const emplace, Verbosity const verbLev, ostream &out
) {
  auto const isDbg = (Verbosity::Debug == verbLev);
  auto const prefix = String("DBG:TMPfillAndEmptySeqHelper-for-classic_and_assoc_seq");

  auto &dest = get<0>(destEtc);
  auto const destTypesName = get<1>(destEtc);

  if (isDbg) {
    out << prefix + ": *before* starting to fill a " + destTypesName << "\n";
  }

  // fill it
  for_each(cbegin(src), cend(src), InsertIntoSeq<Dest>(&dest, emplace, verbLev, &out));

  // empty it
  for_each(cbegin(src), cend(src), RemoveFromSeq<Dest>(&dest, verbLev, &out));

  // sanity-check, is the target really empty?
  if (!empty(dest))
    throwRuntimeError(String(__func__) + " " + destTypesName + " isn't empty");

  if (isDbg) {
    out << "\n" + prefix + ": *after*  finished emptying a " + destTypesName << "\n";
  }
}

} // namespace TMP



template <class Int> class ArithOnly final {
  Int _value;

public:
  using value_type = Int;

  ArithOnly() : _value(Int{}) {}

  explicit ArithOnly(Int value) : _value(value) {}

  constexpr ArithOnly(const ArithOnly &) = default;

#if defined(USE_MOVE)

  ArithOnly(ArithOnly &&that) noexcept
  : _value(std::exchange(that._value, Int{})) {}

  ArithOnly &operator=(ArithOnly &&that) noexcept {
    if (this != &that) {
      _value = std::exchange(that._value, Int{});
    }
    return *this;
  }

#endif

  auto operator<=>(ArithOnly const &) const = default;

  friend ostream &operator<<(ostream &o, ArithOnly const &src) {
    o << (+src.value()); // unary plus makes char-ish template type return numeric value (C++ FAQ Lite online)
    return o;
  }

  decltype(auto) value() const noexcept { return _value; }
};



template <class Int> class ArithWithMemFromHeap final {
  Int      _value; //< only use this for comparing!
  size_t   _dataSize;
  char    *_pchData;

public:
  using value_type = Int;

  void swap(ArithWithMemFromHeap &that) noexcept {
    using std::swap;
    swap(_value, that._value);
    swap(_dataSize, that._dataSize);
    swap(_pchData, that._pchData);
  }

  ~ArithWithMemFromHeap() { delete[] _pchData; }

  ArithWithMemFromHeap() : _value(Int{}), _dataSize(0U), _pchData(nullptr) {}

  explicit ArithWithMemFromHeap(Int inValue)
  : _value(inValue), _dataSize(0U), _pchData(nullptr) {
    auto const payload = randomSentence() + '\0';

    if (empty(payload))
      throwRuntimeError(String(__func__) + " proposed payload is empty");

    _dataSize = payload.size();
    _pchData = new char[_dataSize];
    std::copy(cbegin(payload), cend(payload), _pchData);
  }

  ArithWithMemFromHeap(ArithWithMemFromHeap const &other)
      : _value(other._value), _dataSize(other._dataSize),
        _pchData(new char[_dataSize]) {
    std::copy(other._pchData, other._pchData + _dataSize, _pchData);
  }

  ArithWithMemFromHeap &operator=(ArithWithMemFromHeap const &other) noexcept {
    ArithWithMemFromHeap that(other);
    swap(that);
    return *this;
  }

#if defined(USE_MOVE)

  ArithWithMemFromHeap(ArithWithMemFromHeap &&that) noexcept
      : _value(that._value), _dataSize(that._dataSize),
        _pchData(std::exchange(that._pchData, nullptr)) {}

  ArithWithMemFromHeap &operator=(ArithWithMemFromHeap &&that) noexcept {
    swap(that);
    return *this;
  }

#endif

  auto operator==(ArithWithMemFromHeap const &that) const {
    return value() == that.value();
  }

  std::weak_ordering operator<=>(ArithWithMemFromHeap const &that) const {
    return value() <=> that.value();
  }

  friend ostream &operator<<(ostream &o, ArithWithMemFromHeap const &src) {
    o << '{'
      << " val=" << (+src.value()) << ", size=" << src.dataSize() << ", data='" << src.data() << '\''
      << " } ";
    return o;
  }

  decltype(auto) value() const noexcept { return _value; }

  auto dataSize() const noexcept { return _dataSize; }

  char const *data() const noexcept { return const_cast<char *>(_pchData); }
};

template<class Int> static inline void swap( ArithWithMemFromHeap<Int> &lhs, ArithWithMemFromHeap<Int> &rhs ) noexcept
{
  lhs.swap(rhs);
}



template <class Int> class ArithWithStdVec final {
  Int            _value;
  vector<char>   _data;

public:
  using value_type = Int;

  void swap(ArithWithStdVec<Int> &that) noexcept {
    using std::swap;
    swap(_value, that._value);
    swap(_data, that._data);
  }

  ArithWithStdVec() : _value(Int{}), _data() {}

  explicit ArithWithStdVec(Int inValue) : _value(inValue), _data() {
    auto const payload = randomSentence() + '\0';
    _data.assign(cbegin(payload), cend(payload));
  }

  ArithWithStdVec(ArithWithStdVec const &other)
  : _value(other._value), _data(other._data) {}

  ArithWithStdVec &operator=(ArithWithStdVec const &other) noexcept {
    ArithWithStdVec that(other);
    swap(that);
    return *this;
  }

#if defined(USE_MOVE)

  ArithWithStdVec(ArithWithStdVec &&that) noexcept
  : _value(that._value), _data(std::move(that._data)) {}

  ArithWithStdVec &operator=(ArithWithStdVec &&that) noexcept {
    swap(that);
    return *this;
  }

#endif

  auto operator==(ArithWithStdVec const &that) const {
    return value() == that.value();
  }

  std::weak_ordering operator<=>(ArithWithStdVec const &that) const {
    return value() <=> that.value();
  }

  friend ostream &operator<<(ostream &o, ArithWithStdVec const &src) {
    o << '{' << " val=" << (+src.value()) << ", size=" << src.dataSize() << ", data='" << src.data() << '\'' << " } ";
    return o;
  }

  decltype(auto) value() const noexcept { return _value; }

  auto dataSize() const noexcept { return _data.size(); }

  char const *data() const noexcept { return const_cast<char *>(&(_data[0])); }
};

template <class Int> static inline void swap(ArithWithStdVec<Int> &lhs, ArithWithStdVec<Int> &rhs) noexcept {
  lhs.swap(rhs);
}



template <class Int> struct MyHash final {

  auto operator()(Int x) const noexcept { return hash<Int>()(x); }

  auto operator()(ArithOnly<Int> const &x) const noexcept {
    return hash<Int>{}(x.value());
  }

  auto operator()(ArithWithMemFromHeap<Int> const &x) const noexcept {
    auto h1 = hash<Int>{}(x.value());
    auto h2 = hash<size_t>{}(x.dataSize());
    auto h3 = hash<char *>{}(const_cast<char *>(x.data()));
    return h1 ^ (h2 << 1) ^ (h3 << 2);
  }

  auto operator()(ArithWithStdVec<Int> const &x) const noexcept {
    auto h1 = hash<Int>{}(x.value());
    auto h2 = hash<size_t>{}(x.dataSize());
    auto h3 = hash<char *>{}(const_cast<char *>(x.data()));
    return h1 ^ (h2 << 1) ^ (h3 << 2);
  }
};



enum class SeqToDo {
  GenerateInsertSeq,
  FillAndEmptySeq,
  CheckTargetSeqMaxSize,
  CheckTargetSeqMaxUniqueSize
};



class App;          // forward declare
class BasicType {}; //< clarity
using AppOutput = tuple<App &, ostream &>;
bool prepare(BasicType, AppOutput &, SeqToDo const); // forward declare



struct LoopCount final {
  ULongLong _val = 0ULL;

  void set( decltype(_val) val) noexcept {
    _val = val;
  }
};



struct GoodRunCount final {
  ULongLong _val = 0ULL;

  void set( decltype(_val) val) noexcept {
    _val = val;
  }
};


class TimedTask final {
  LoopCount      _loopCnt;
  GoodRunCount   _goodRunCnt;

public:
  MT::TimePoint _start;
  MT::TimePoint _end;

  auto loopCount() const noexcept { return _loopCnt; }
  auto goodRunCount() const noexcept { return _goodRunCnt; }

  void setLoopCount(ULongLong const v) noexcept { _loopCnt.set(v); }

  void incrementGoodRunCount() { ++_goodRunCnt._val; }

  template <class Dest, class Src>
  void run(StringAnd<Dest> &, Src const &, AppOutput &);
};



struct RawishOption final {
  String _name;
  String _value;
}; ///< for rawish option


auto toString(RawishOption const &v) {
  String res;
  const auto quote = String("'");

  if (empty(v._name)) {
    res = String("<unnamed>");
  } else {
    res = (quote + v._name + quote);
  }

  if (!empty(v._value)) {
    res += (quote + v._value + quote);
  }

  return res;
}



struct Errors final {
  StringSeq _values;
};


ostream &operator<<(ostream &o, Errors const &v) {
  bool output(false);
  auto const first = cbegin(v._values);
  for(auto cit = first; cit != cend(v._values); ++cit, output=true){
    o << ( (first == cit) ? "" : ",") << (*cit);
  }
  if(output){
    o << "\n";
  }

  return o;
}



static auto tryParseAsBool(char const *src) {
  optional<bool> res;

  if (nullptr != src) {
    switch (auto const ch = *src; ch) {
    case 't':
    case 'T':
    case '1':
      res = true;
      break;

    case 'f':
    case 'F':
    case '0':
      res = false;
      break;
    }
  }

  return res;
}



template <class TMap> static auto tryParseImp(StringView const src, TMap const &myMap) {
  optional<typename TMap::key_type> res;
  for (auto cit = cbegin(myMap); cit != cend(myMap); ++cit) {

    if (cit->second == src) { // note: ensure that ptr's are not compared by address, but lexicographically
      res = cit->first;
    }

  }

  return res;
}



enum class TargetSeq { //< target sequence types
  Unset,
  Deque,
  List,
  Set,
  Vector,
  UnorderedSet
};


static auto targetSeqMap() {
  using enum TargetSeq;
  EnumNameMap<TargetSeq> result{
    {Unset,        "unset"},
    {Deque,        "deque"},
    {List,         "list"},
    {Set,          "set"},
    {Vector,       "vector"},
    {UnorderedSet, "unordered_set"}};
  return result;
}


static auto toCharP(TargetSeq const v) { return toCharPImp(targetSeqMap(), v); }


static auto tryParseAsTargetSeq(char const *src) {
  auto res = tryParseImp(src, targetSeqMap());
  return res;
}



enum Basic {
  bsChar,
  bsSChar,
  bsUChar,
  bsWChar_t,
  bsChar16_t,
  bsChar32_t,
  bsSShort,
  bsUShort,
  bsSInt,
  bsUInt,
  bsSLong,
  bsULong,
  bsSLongLong,
  bsULongLong
};


static auto basicTypeMap() {
  EnumNameMap<Basic> result{
    {bsChar,      "Char"},
    {bsSChar,     "SignedChar"},
    {bsUChar,     "UnsignedChar"},
    {bsWChar_t,   "WChar_t"},
    {bsChar16_t,  "Char16_t"},
    {bsChar32_t,  "Char32_t"},
    {bsSShort,    "SignedShort"},
    {bsUShort,    "UnsignedShort"},
    {bsSInt,      "SignedInt"},
    {bsUInt,      "UnsignedInt"},
    {bsSLong,     "SignedLong"},
    {bsULong,     "UnsignedLong"},
    {bsSLongLong, "SignedLongLong"},
    {bsULongLong, "UnsignedLongLong"}};
  return result;
}


static auto toCharP(Basic const v)
{
   return toCharPImp(basicTypeMap(), v);
}


static auto tryParseAsBasic(String const &src) {
  optional<Basic> res;

  if (src.contains(String("wchar_t"))) {
    res = bsWChar_t;
  }

  if ( !res && src.contains(String("char16_t"))) {
    res = bsChar16_t;
  }

  if ( !res && src.contains(String("char32_t"))) {
    res = bsChar32_t;
  }

  String const unsignedStr(String("unsigned"));
  String const longStr(String("long"));

  auto unsignedPos = src.find(unsignedStr);
  auto signedPos = src.find(String("signed"), (unsignedPos == String::npos) ? 0u : unsignedStr.size());
  // break tie if needed
  if ( !res && unsignedPos != String::npos && signedPos != String::npos) {
    *((unsignedPos > signedPos) ? &signedPos : &unsignedPos) = String::npos;
  }

  auto const shortPos = src.find(String("short"));
  auto const charPos = src.find(String("char"));
  auto const longPosFirst = src.find(longStr);
  auto const longPosSecond = (longPosFirst == String::npos) ? longPosFirst : src.find(longStr, longStr.size());
  auto const intPos = src.find(String("int"));

  bool const hasUnsigned = (unsignedPos != String::npos);
  bool const hasSigned = (signedPos != String::npos);

  if ( !res && charPos != String::npos) {

    if (hasUnsigned) {
      res = bsUChar;
    } else {
      res = (hasSigned) ? bsSChar : bsChar;
    }

  }

  if ( !res && shortPos != String::npos) {
    res = (hasUnsigned) ? bsUShort : bsSShort;
  }

  if ( !res && longPosFirst != String::npos) {

    if (longPosSecond == String::npos) {
      res = (hasUnsigned) ? bsULong : bsSLong;
    } else {
      res = (hasUnsigned) ? bsULongLong : bsSLongLong;
    }

  }

  if ( !res && hasSigned && (!hasUnsigned)) {
    res = bsSInt;
  }

  if ( !res && hasUnsigned && (!hasSigned)) {
    res = bsUInt;
  }

  if ( !res && intPos != String::npos) {
    res = bsSInt;
  }

  return res;
}



enum class EWrap { ///< wrapping-type (if any) // TODO: define more of these?
  ArithOnly,
  ArithWithMemFromHeap,
  ArithWithStdVec,
  None
};


static auto wrapMap() {
  using enum EWrap;
  EnumNameMap<EWrap> result{
    {ArithOnly,            "arithonly"},
    {ArithWithMemFromHeap, "arithwithmemfromheap"},
    {ArithWithStdVec,      "arithwithstdvec"},
    {None,                 "none"}};
  return result;
}

static auto toCharP(EWrap const v) { return toCharPImp(wrapMap(), v); }

static auto tryParseAsEWrap(char const *src) {
  return tryParseImp(src, wrapMap());
}



enum class EToDo { ///< things this app may need to do
  Nothing,
  DealWithIncorrectParams,
  GenInsertSeq,
  FillAndEmptySeq
};


static auto toDoMap() {
  using enum EToDo;
  EnumNameMap<EToDo> result{
    {Nothing,                 "do-nothing"},
    {DealWithIncorrectParams, "deal-with-incorrect-params"},
    {GenInsertSeq,            "generate-insert-sequence"},
    {FillAndEmptySeq,         "fill-and-empty-sequence"},
  };
  return result;
}


static auto toCharP(EToDo const v) { return toCharPImp(toDoMap(), v); }



struct InsertCount final {
  ULongLong _val = 0ULL;

  void set( decltype(_val) val) noexcept {
    _val = val;
  }
};



class App final {
  InsertCount  _insertCnt;
  LoopCount  _loopCnt;
  TargetSeq _targetSeqType = TargetSeq::Unset;
  optional<Basic> _basicType;
  EWrap  _wrap = EWrap::None;
  std::filesystem::path _inputFilename;
  std::filesystem::path _outputFilename;
  EToDo  _toDo = EToDo::Nothing;
  Verbosity _verbosity = Verbosity::None;
  bool _emplace = true; //< use emplace-ish fn (instead of push_back-ish fn)
  bool _initInsertSeqSucceeded = false;
  bool _mayExit = false; //< indicate if app mayExit

public:
  auto inputFilename() const { return _inputFilename; }

  void setInputFilename(std::filesystem::path const &v) { _inputFilename = v; }

  auto outputFilename() const { return _outputFilename; }

  void setOutputFilename(std::filesystem::path const &v) {
    _outputFilename = v;
  }

  auto insertCount() const noexcept { return _insertCnt; }

  void setInsertCount(ULongLong v) noexcept { _insertCnt.set(v); }

  auto loopCount() const noexcept { return _loopCnt; }

  void setLoopCount(ULongLong v) noexcept { _loopCnt.set(v); }

  auto toDo() const noexcept { return _toDo; }

  void setToDo(EToDo v) noexcept { _toDo = v; }

  auto targetSeqType() const noexcept { return _targetSeqType; }

  void setTargetSeqType(TargetSeq v) noexcept { _targetSeqType = v; }

  bool isTargetSeqTypeUnset() const noexcept { return _targetSeqType == TargetSeq::Unset; }

  auto verbosity() const noexcept { return _verbosity; }

  void incrementVerbosity() noexcept {
    switch (_verbosity) {

    case Verbosity::None:
      _verbosity = Verbosity::Verbose;
      break;

    case Verbosity::Verbose:
      _verbosity = Verbosity::Debug;
      break;

    case Verbosity::Debug:
      // don't do anything (Debug is the maximum verbosity level)
      // (case included to get rid of warning)
      break;
    }
  }

  bool isVerbose() const noexcept { return Verbosity::None != verbosity(); }

  auto emplace() const noexcept { return _emplace; }

  void setEmplace(bool const v) noexcept { _emplace = v; }

  auto basicType() const noexcept { return _basicType; }

  void setBasicType(Basic const v) noexcept { _basicType = v; }

  auto wrap() const noexcept { return _wrap; }

  void setWrap(EWrap const v) noexcept { _wrap = v; }


  bool initInsertSeqSucceeded() const noexcept { return _initInsertSeqSucceeded; }

  void setInitInsertSeqSucceeded(bool const v) noexcept { _initInsertSeqSucceeded = v; }


  bool mayExit() const noexcept { return _mayExit; }

  void setMayExit(bool const v) noexcept { _mayExit = v; }


  template <class Dest, class Src>
  static void fillAndEmptyTargetSeq(AppOutput &ao, StringAnd<Dest> &das, Src const &src) {
    TMP::fillAndEmptySeq(das, src, get<0>(ao).emplace(), get<0>(ao).verbosity(), get<1>(ao));
  }

  void dumpState(std::ostream &out) const {
    char const quote = '\'';
    auto const sep = StringView(": ");
    auto const nl = StringView("\n");
    auto const notSet = StringView("<not set>");
    auto const empty = StringView();

    out << StringView("{") << nl
        << StringView("InputFilename") << sep << quote << (inputFilename().empty() ? empty : inputFilename().string()) << quote << nl
        << StringView("OutputFilename") << sep << quote << (outputFilename().empty() ? empty : outputFilename().string()) << quote << nl
        << StringView("InsertCount") << sep << to_string(insertCount()._val) << nl
        << StringView("LoopCount") << sep << to_string(loopCount()._val) << nl
        << StringView("to-do") << sep << toCharP(toDo()).value() << nl
        << StringView("TargetSeqType") << sep << ( toCharP(targetSeqType()).value_or(notSet.data()) ) << nl
        << format("isTargetSeqTypeUnset{}{}", sep, isTargetSeqTypeUnset()) << nl
        << StringView("Verbose") << sep << toCharP(verbosity()).value_or("<verbosity-map-fail>") << nl
        << format("isVerbose{}{}", sep, isVerbose()) << nl
        << StringView("BasicType") << sep << (basicType() ? toCharP(*basicType()).value_or("<basicType-map-fail>") : notSet.data() ) << nl
        << StringView("Wrap") << sep << toCharP(wrap()).value() << nl
        << format("emplace{}{}", sep, emplace()) << nl
        << format("init-insertion-seq-succeeded{}{}", sep, initInsertSeqSucceeded()) << nl
        << format("mayExit{}{}", sep, mayExit()) << nl
        << StringView("}") << nl;
    }
};

void dealArgParseFail(RawishOption const &v, AppOutput &ao) {
  if (get<0>(ao).isVerbose()) {
    get<1>(ao) << String("failed to parse '") + v._name + "' with value '" + v._value + "'\n";
  }
}

void dealMultipleAssign(String const &prefix, String const &name, AppOutput &ao) {
  if (get<0>(ao).isVerbose()) {
    get<1>(ao) << prefix + ": '" + name + "' specified more than once, using last value\n";
  }
}

void dealNumericParseFail(RawishOption const &v, AppOutput &ao) {
  if (get<0>(ao).isVerbose()) {
    get<1>(ao) << "Parsing as a number failed for '" + v._name + "' (is it non-numeric, too small, or too large?).\n";
  }
}

void dealParamsIncorrect(AppOutput &ao) {
  if (get<0>(ao).isVerbose()) {
    get<1>(ao) << "The supplied parameters were incomplete or incorrect, bailing.\n";
  }
}

auto unknownToString(RawishOption const &op) {
  auto const quote = String("'");
  return String("unknown parameter '") + op._name + String("': ") + quote + op._value + quote;
}

auto unprefixedToString(RawishOption const &op) {
  auto const quote = String("'");
  return String("unprefixed: ") + quote + op._value + quote;
}

void dealParamsUnprefixedOrUnknown(Errors const &errors, AppOutput &ao) {
  auto cit = cbegin(errors._values);
  if (get<0>(ao).isVerbose()) {
    bool first(true);
    for(; cit != cend(errors._values); ++cit){
      get<1>(ao) << (first ? "" : ",") << *cit;
      first = false;
    }
    if(!first) {
      get<1>(ao) << "\n";
    }
  }
}



namespace CL {

class NamedOption final {
  using DealFn = void (*)(RawishOption &, AppOutput &);
  static void DoNothing(RawishOption &, AppOutput &) {}
  StringViewSeq _keys; //< require: this set may not be empty
  StringView _shorts;
  DealFn _fn = DoNothing;

public:
  NamedOption() = default;

  NamedOption(std::initializer_list<StringView> longs, StringView shorts, decltype(_fn) fn)
  : _keys(longs), _shorts(shorts), _fn(fn) {
    if (empty(_keys)) {
      throwRuntimeError(String(__func__) + ": the long names sequence is empty");
    }
  }

  auto firstKey() const { return _keys.front(); }

  auto function() const { return (_fn == nullptr) ? DoNothing : _fn; }

  auto names() const {
    vector<StringView> res(cbegin(_keys), cend(_keys));

    for (String::size_type i = 0u; i < _shorts.size(); ++i) {
      res.push_back(StringView(_shorts.data() + i, 1u));
    }
    return res;
  }
};


static auto createHelp(StringView appName) {
  return std::array<String, 19>{
      String("Usage: ") + String(appName) + String(" <all required options>"),
      " [-verbose|v]                                 ++verbosity (additive)",
      " [-dbg-parse|dp]                              show arg parse results "
      "only",
      " [-help|h|?]                                  write this info",
      "  -type|t insert-type's basic-type*           "
      "([[un]signed]char|short|int|(long[long]))|wchar_t|(char(16|32)_t)",
      "  -wrap|w wrapping-type*                      "
      "arithOnly|arithWith(MemFromHeap|StdVec)|none",
      " [-loop-num|l|r loop count*]                  <loop count>",
      "  -insert-num|n number of inserts per loop*   <inserts per loop count>",
      " [-input|i insert-seq source filename]        <input filename>",
      " [-output|o insert-seq destination filename]  <output filename>",
      " [-gen-inserts|gi]                            generate insert-seq only",
      "  -target-type|tt target-seq type*            "
      "vector|list|set|deque|unordered_set",
      " [-use-emplace|ue]                            use emplace (as opposed "
      "to push_back) functionality",
      "",
      "*  Required argument.",
  };
}


static auto CommandLineOptions() {
  static vector<NamedOption> res{
    NamedOption({"verbose"}, {"v"},
      [](RawishOption &, AppOutput &ao) { get<0>(ao).incrementVerbosity(); }),
    NamedOption({"dbg-parse", "dp"}, {},
      [](RawishOption &, AppOutput &ao) {
        get<0>(ao).dumpState( get<1>(ao) ); get<0>(ao).setMayExit(true); }),
    NamedOption({"help"}, {"h?"},
      [](RawishOption &, AppOutput &ao) {
        auto const lines = createHelp("mbc");
        for(auto cit = cbegin(lines); cit != cend(lines); ++cit){
	  get<1>(ao) << *cit << "\n";
        }
	get<1>(ao) << "\n";
	get<0>(ao).setMayExit(true); }),
    NamedOption({"type"}, {"t"},
      [](RawishOption &opt, AppOutput &ao) {
        auto pr = tryParseAsBasic(opt._value); pr ? get<0>(ao).setBasicType(*pr) : dealArgParseFail(opt, ao); }),
    NamedOption({"wrap"}, {"w"},
      [](RawishOption &opt, AppOutput &ao) {
        auto pr = tryParseAsEWrap(opt._value.data()); pr ? get<0>(ao).setWrap(*pr) : dealArgParseFail(opt, ao); }),
    NamedOption({"loop-num"}, {"lr"},
      [](RawishOption &opt, AppOutput &ao) {
        auto cr = tryConvertAs(ULongLong{}, opt._value); cr ? get<0>(ao).setLoopCount(*cr) : dealNumericParseFail(opt, ao); }),
    NamedOption({"insert-num"}, {"n"},
      [](RawishOption &opt, AppOutput &ao) {
        auto cr = tryConvertAs(ULongLong{}, opt._value); cr ? get<0>(ao).setInsertCount(*cr) : dealNumericParseFail(opt, ao); }),
    NamedOption({"input"}, {"i"},
      [](RawishOption &opt, AppOutput &ao) { get<0>(ao).setInputFilename(opt._value); }),
    NamedOption({"output"}, {"o"},
      [](RawishOption &opt, AppOutput &ao) { get<0>(ao).setOutputFilename(opt._value); }),
    NamedOption({"fill-and-empty-seq", "faes"}, {},
      [](RawishOption &, AppOutput &ao) { get<0>(ao).setToDo(EToDo::FillAndEmptySeq); }),
    NamedOption({"gen-inserts", "gi"}, {},
      [](RawishOption &, AppOutput &ao) { get<0>(ao).setToDo(EToDo::GenInsertSeq); }),
    NamedOption({"target-type", "tt"}, {},
      [](RawishOption &opt, AppOutput &ao) {
        auto pr = tryParseAsTargetSeq(opt._value.data()); pr ? get<0>(ao).setTargetSeqType(*pr) : dealArgParseFail(opt, ao); }),
    NamedOption({"use-emplace", "ue"}, {},
      [](RawishOption &opt, AppOutput &ao) {
	 auto pr = tryParseAsBool(opt._value.data()); pr ? get<0>(ao).setEmplace(*pr) : dealArgParseFail(opt, ao); }),
  };
  return res;
}


/// @remark map names to NamedOptions
static auto findNamedOption(RawishOption &opt, AppOutput &ao) {
  auto cmdLineOpts = CommandLineOptions();

  if (empty(cmdLineOpts)) {
    throwRuntimeError(String(__func__) + " empty NamedOption seq");
  }

  tuple result(false, NamedOption());
  for (auto &clo : cmdLineOpts) {
    auto const names = clo.names();
    tuple nameRange(begin(names), end(names));

    if (get<0>(nameRange) == get<1>(nameRange)) {
      throwRuntimeError(String(__func__) + " NamedOption's nameset is empty");
    }

    for (; get<0>(nameRange) != get<1>(nameRange); ++get<0>(nameRange)) {
      if (opt._name == *get<0>(nameRange)) {
        get<0>(result) = true;
        get<1>(result) = clo;
        return result;
      }
    }
  }

  // to reach this code ==> option is unknown / empty???
  if (get<0>(ao).isVerbose()) {
    get<1>(ao) << String(__func__) + ": unhandled option " + toString(opt) + "\n";
  }

  return result;
}


static void parseSingle(RawishOption &opt, Errors &errors, AppOutput &ao) {
  if (empty(opt._name)) {
    errors._values.push_back(unprefixedToString(opt));
  } else {
    auto [foundIt, namedOption] = findNamedOption(opt, ao);

    if (foundIt) {
      // ignore when option should have value but doesn't

      auto fn = namedOption.function();
      fn(opt, ao);
    }

  }
}


} // namespace CL


/*
   TODO  debug to see if the new use-emplace functionality works

   TODO  harden input params against fuzzing-like 'attacks'?

   TODO  add support for basic_string targets when arith-type is one of the char-ish ones?

   TODO  add support to drop first-n results (so that hot and cold code paths become seperable)

   TODO  add debug-extrapolation-value functionality?

   TODO  stretch goal: remove all ifdef's (if possible)

   TODO  support more diverse usable types, such as:
         * ? mix integral type with one or more different std-string-like types ?
*/



struct ParseInfo final {
  const String _prefixLong;   // leave these 4 as String (i.s.o. StringView) to avoid referencing issues
  const String _prefixShort;
  const String _stopper;
  const String _assign;

  int _nullCnt = 0;
  int _emptyCnt = 0;
  int _partAfterAssignEmptyCnt = 0;
  bool _sawStopper = false;
  bool _tooFewArgs = false;

  ParseInfo(const String &prefixLong, const String &prefixShort, const String &stopper, const String &assign)
  : _prefixLong(prefixLong), _prefixShort(prefixShort), _stopper(stopper), _assign(assign) {}
};


static void parseOneParam(ParseInfo &info, int argc, char *argv[], AppOutput &ao, Errors &errors, int &i) {
  // (nullptr should be impossible, but programmed input might be bad)
  if (argv[i] == nullptr) {
    ++info._nullCnt;
    return;
  }

  auto const val = String(argv[i]);
  if (empty(val)) {
    ++info._emptyCnt;
    return;
  }

  if (info._stopper == val) {
    info._sawStopper = true;
    return;
  }

  bool const startsWLong = val.starts_with(info._prefixLong);
  bool const startsWShort = val.starts_with(info._prefixShort);
  if (!startsWLong && !startsWShort) {
    // TODO: warn about non-prefixed option?
    return;
  }

  auto const &prefix = startsWLong ? info._prefixLong : info._prefixShort;
  if (empty(prefix)) {
    throwRuntimeError(String(__func__) + " prefix is empty");
  }

  RawishOption opt;
  if (auto const idxAssign = val.find(info._assign, prefix.size());
      idxAssign == String::npos) { // no assign
    opt._name = val.substr(prefix.size());
    // copy next-val iif it exists AND does NOT start-with an option-prefix
    if (i + 1 < argc && argv[i + 1] != nullptr) {
      String const nxt(argv[i + 1]);
      if (!nxt.starts_with(info._prefixLong) &&
          !nxt.starts_with(info._prefixShort)) {
        opt._value = nxt;
        ++i;
      }
    } else {
      opt._value.clear();
    }
    CL::parseSingle(opt, errors, ao);
  } else { // has assign
    opt._name = val.substr(prefix.size(), idxAssign - prefix.size());
    opt._value = val.substr(idxAssign + info._assign.size());
    if (empty(opt._value)) {
      ++info._partAfterAssignEmptyCnt;
    } else {
      CL::parseSingle(opt, errors, ao);
    }
  }
}


static void parse(ParseInfo &info, int argc, char *argv[], AppOutput &ao, Errors &errors) {
  // have enough args been supplied?
  if (argc <= 1) {
    info._tooFewArgs = true;
    return;
  }

  for (int i = 1; i < argc; ++i) {
    parseOneParam(info, argc, argv, ao, errors, i);
    if (info._sawStopper) {
      break;
    }
  }
}


static void parse(int argc, char *argv[], AppOutput &ao, Errors &errors) {
  String const longPrefix("--");
  String const shortPrefix("-");
  String const stopper("--");
  String const assign("=");
  ParseInfo parseInfo(longPrefix, shortPrefix, stopper, assign);
  parse(parseInfo, argc, argv, ao, errors);

  // have enough args been supplied?
  if (parseInfo._tooFewArgs) {
    if (get<0>(ao).isVerbose()) {
      get<1>(ao) << String("error: too few arguments given.\n");
    }
    return;
  }

  if (get<0>(ao).isVerbose()) {
    auto const seenEnd = " seen!\n";
    auto const preamble = String("error: ");

    if (parseInfo._nullCnt > 0) {
      get<1>(ao) << preamble + (parseInfo._nullCnt == 1 ? "a null argument was" : "multiple null arguments were") + seenEnd;
    }

    if (parseInfo._emptyCnt > 0) {
      get<1>(ao) << preamble + (parseInfo._emptyCnt == 1 ? "an empty argument was" : "multiple empty arguments were") + seenEnd;
    }

    if (parseInfo._partAfterAssignEmptyCnt > 0) {
      String phrase(
        (parseInfo._partAfterAssignEmptyCnt == 1) ? "an option with an empty assign was" : "multiple options with empty assigns were");
      get<1>(ao) << (preamble + phrase + seenEnd);
    }
  }
}


template <class Dest, class Src> void TimedTask::run(StringAnd<Dest> &dan, Src const &src, AppOutput &ao) {
  App::fillAndEmptyTargetSeq(ao, dan, src);
}


template <class Dest, class Src> static void timeRepeatableTask(StringAnd<Dest> &dan, Src const &src, AppOutput &ao, TimedTask &task) {
  auto count = task.loopCount();

  task._start = MT::calcNow();

  for (; count._val; --count._val) {
    task.run(dan, src, ao);
    task.incrementGoodRunCount();
  }

  task._end = MT::calcNow();
}


static bool writeTimedInfo(AppOutput &ao, ostringstream &dest, TimedTask const &task) {
  if (task._start > task._end) {
    get<1>(ao) << "Error: task.time-at-start is greater than time-at-finish\n";
    return false;
  }

  char const chDoubleQuote('"');
  char const elemSep(' ');
  dest << "To insert-and-remove ";
  dest << get<0>(ao).insertCount()._val;
  dest << elemSep;
  dest << chDoubleQuote << toCharP(*get<0>(ao).basicType()).value_or("<basicType-map-fail>") << chDoubleQuote;
  if (get<0>(ao).insertCount()._val > 1)
    dest << "s";

  dest << elemSep;
  dest << chDoubleQuote << toCharP(get<0>(ao).wrap()).value() << chDoubleQuote;
  if (get<0>(ao).insertCount()._val > 1)
    dest << "s";

  dest << elemSep << get<0>(ao).loopCount()._val << " times into a ";
  auto const locTargetSeqType = get<0>(ao).targetSeqType();
  dest << toCharP(locTargetSeqType).value_or("<targetSeqType-map-fail>");

  auto const inpFname = get<0>(ao).inputFilename();
  dest << elemSep << chDoubleQuote;

  if (!inpFname.empty())
    dest << String("from '") + inpFname.string().c_str() + "'";

  dest << chDoubleQuote << " required ";
  auto const timeInMs = MT::timeInMs(task._start, task._end);
  dest << timeInMs << " ms";

  // output the 'horsepower'
  // (or nr of 'conceptual' insert-and-removes per time-unit)
  auto const appInsertCount = get<0>(ao).insertCount();
  auto const appLoopCount = get<0>(ao).loopCount();
  long double const horsePower = 1.000L * appInsertCount._val * appLoopCount._val / timeInMs;

  dest << " achieving ";
  withPrecision(dest, horsePower, 9L);
  dest << " 'conceptual' insert-and-removes per ms" << '\n';
  return true;
}



template <class C> static void calcRandomNumbers(C &dest, ULongLong const howMany) {
  std::random_device rd;
  std::mt19937_64 re(rd());

  using ValT = typename C::value_type;
  using InsertVec = vector<ValT>;
  using InsertSet = set<ValT>;

  InsertSet seen;
  InsertVec vec;
  vec.reserve(narrowCast<typename C::size_type>(howMany));

  auto const valMin = std::numeric_limits<ValT>::lowest();
  auto const valMax = std::numeric_limits<ValT>::max();

  while (vec.size() < howMany) {
    auto const i = myRandint(valMin, valMax, re);

    // may need TODO something here to prevent infinite seeming loop (when few-to-one unseen num(s) needs to be found)
    if (seen.find(i) == end(seen)) {
      vec.push_back(i);
      seen.insert(i);
    }
  }

  dest.assign(cbegin(vec), cend(vec));
  // TODO: test this
}



template <class Int> static void outputValue(Int const val, AppOutput &ao) {
  if (std::is_signed_v<Int>) {
    get<1>(ao) << static_cast<LongLong>(val);
  } else {
    get<1>(ao) << static_cast<ULongLong>(val);
  }
}



template <class C> static bool readFromFile(AppOutput &ao, C &dest) {
  auto const desiredSize = get<0>(ao).insertCount();
  auto const initialSize = dest.size();
  // does the target sequence already contain enough elements?
  if (initialSize >= desiredSize._val)
    return false;

  using ValT = typename C::value_type;
  std::unordered_set<ValT> known(begin(dest), end(dest));
  C inserts;

  auto const inpFname = get<0>(ao).inputFilename();
  std::ifstream ifs(inpFname);
  auto const verbo = get<0>(ao).verbosity();
  if (!ifs) {
    if (Verbosity::None != verbo) {
      get<1>(ao) << String("Error: failed to open file '") + inpFname.string().c_str() + "' to read insert values, bailing.\n";
    }
    get<0>(ao).setInitInsertSeqSucceeded(false);
    return false;
  }

  std::string line;
  auto const isDbg = (Verbosity::Debug == verbo);
  for (; ifs;) {
    line.clear();
    getline(ifs, line);

    if (!empty(line)) {

      if (isDbg) {
        get<1>(ao) << "DBG: read line: '" + line + "'.\n";
      }

      if (auto valOrFail = tryConvertAs(ValT{}, line); valOrFail) {

        if (isDbg) {
          get<1>(ao) << "DBG: successfully parsed '" + line + "' into number ";
          outputValue(*valOrFail, ao);
          get<1>(ao) << ".\n";
        }

        if (end(known) == known.find(*valOrFail)) {
          inserts.push_back(*valOrFail);
          known.insert(*valOrFail);
        } else {

          if (isDbg) {
            get<1>(ao) << "DBG: number ";
            outputValue(*valOrFail, ao);
            get<1>(ao) << " appears to be a duplicate, ignoring it.\n";
          }
        }

      } else {
        if (isDbg) {
          get<1>(ao) << "DBG: conversion-to-number failed for '" << line << "' .\n";
        }
      }

    } // EOIf line-not-empty

  } // EOFor against ifs

  dest.insert(end(dest), begin(inserts), end(inserts));
  get<0>(ao).setInitInsertSeqSucceeded(true);
  return true;
}



template <class C> static bool initialize(AppOutput &ao, C &target) {
  calcRandomNumbers(target, get<0>(ao).insertCount()._val);
  const bool res = (target.size() == get<0>(ao).insertCount()._val);
  get<0>(ao).setInitInsertSeqSucceeded(res);
  return res;
}



template <class C> static bool generateInsertSeq(AppOutput &ao, C &dest) {
  return (!initialize(ao, dest));
}



template <class C> static bool writeInsertSeq(AppOutput &ao, C &src) {
  auto const ofname = get<0>(ao).outputFilename();
  auto const toFile = ((!empty(ofname)) && ofname != String("-"));
  bool const verbo = get<0>(ao).isVerbose();

  std::ofstream outputFile;

  if (toFile) {
    outputFile.open(ofname, std::ios::app);

    if (!outputFile) {
      if (verbo) {
        get<1>(ao) << String("error: could not open file '") + ofname.string().c_str() + "', bailing\n";
      }
      return false;
    }
  }

  ostream &dest = toFile ? get<1>(ao) : outputFile;

  for (auto const &e : src) {
    dest << basicToStr(e) + String("\n");
  }

  if (!dest) {
    if (verbo) {
      get<1>(ao) << String("error: writing to ") +
                      (toFile ? String("file '") + ofname.string().c_str() + String("'") : String("output stream")) +
                      String(" failed!\n");
    }
    return false;
  }

  return true;
}



template <class Dest, class Src> bool fillAndEmpty(StringAnd<Dest> &dtn, Src &src, AppOutput &ao) {

  if (empty(get<0>(ao).inputFilename())) {
    initialize(ao, src);
  } else {
    readFromFile(ao, src);
  }

  bool const result(get<0>(ao).initInsertSeqSucceeded());
  if (result) {
    bool const verbo = get<0>(ao).isVerbose();
    TimedTask ttFillAndEmpty;
    get<0>(ao).setToDo(EToDo::FillAndEmptySeq);
    auto loopCntInit = get<0>(ao).loopCount();
    if(loopCntInit._val != 0ULL){
      if (verbo) {
        get<1>(ao) << String("error: the required loopCount was not set, bailing\n");
      }
      return false;
    }

    ttFillAndEmpty.setLoopCount(get<0>(ao).loopCount()._val);
    timeRepeatableTask(dtn, src, ao, ttFillAndEmpty);
    ostringstream oss;
    if (writeTimedInfo(ao, oss, ttFillAndEmpty))
      get<1>(ao) << oss.str();
  }

  return result;
}


template <class C> bool exceedsTargetMaxSize(C const &target, App const &app) {
  return (app.insertCount()._val > target.max_size());
}



template <class Num> static bool exceedsTargetMaxUniqueSizeImp(Num const, ULongLong const insertCount) {
  auto const valMax = std::numeric_limits<Num>::max();
  auto term = static_cast<ULongLong>(valMax);

  if (std::is_signed<Num>::value) {
    ++term;
    term *= 2ULL;
  }

  return insertCount > term;
}



template <class C> auto exceedsTargetMaxUniqueSize(C const &, App const &app, int)
  -> decltype(typename C::value_type::value_type(), bool()) {
  using ValT = typename C::value_type::value_type;
  return exceedsTargetMaxUniqueSizeImp(ValT{}, app.insertCount()._val);
}



template <class C> bool exceedsTargetMaxUniqueSize(C const &, App const &app, long) {
  using ValT = typename C::value_type;
  return exceedsTargetMaxUniqueSizeImp(ValT{}, app.insertCount()._val);
}



template <class Dest, class Src> bool choose(StringAnd<Dest> &dtn, Src &src, AppOutput &ao, SeqToDo const toDo) {
  auto &dest = get<0>(dtn);

  switch (toDo) {
    case SeqToDo::GenerateInsertSeq:
      return generateInsertSeq(ao, src) && writeInsertSeq(ao, src);

    case SeqToDo::FillAndEmptySeq:
      return fillAndEmpty(dtn, src, ao);

    case SeqToDo::CheckTargetSeqMaxSize:
      return !exceedsTargetMaxSize(dest, get<0>(ao));

    case SeqToDo::CheckTargetSeqMaxUniqueSize:
      return !exceedsTargetMaxUniqueSize(dest, get<0>(ao), 6);
  }

  throwRuntimeError(String(__func__) + " " + unexpectedValue(toDo));
}



template <class Basic, class Wrap> bool prepare(
  Basic /* target's basic type */, Wrap /* Wrap-type value */, AppOutput &ao, SeqToDo const toDo) {
  bool const verbo = get<0>(ao).isVerbose();

  auto const targetSeqType = get<0>(ao).targetSeqType();
  if(targetSeqType == TargetSeq::Unset) {
    if(verbo) {
      get<1>(ao) << String("error: targetSeqType has not been set, bailing\n");
    }
    return false;
  }

  auto const targetSeqTypeName = toCharP(targetSeqType);
  if(!targetSeqTypeName) {
    if(verbo) {
      get<1>(ao) << String("error: failed to map targetSeqTypeName to a valid value, bailing\n");
    }
    return false;
  }

  String const  targetSeqTypeNameStr(*targetSeqTypeName);
  vector<Basic> inserts;

  switch (targetSeqType) {
    using enum TargetSeq;

    case Vector: {
      vector<Wrap> target;
      tuple dtn(target, targetSeqTypeNameStr);
      return choose(dtn, inserts, ao, toDo);
    }

    case Deque: {
      std::deque<Wrap> target;
      tuple dtn(target, targetSeqTypeNameStr);
      return choose(dtn, inserts, ao, toDo);
    }

    case List: {
      std::list<Wrap> target;
      tuple dtn(target, targetSeqTypeNameStr);
      return choose(dtn, inserts, ao, toDo);
    }

    case Set: {
      set<Wrap> target;
      tuple dtn(target, targetSeqTypeNameStr);
      return choose(dtn, inserts, ao, toDo);
    }

    case UnorderedSet: {
      std::unordered_set<Wrap, MyHash<Basic>> target;
      tuple dtn(target, targetSeqTypeNameStr);
      return choose(dtn, inserts, ao, toDo);
    }

    default:
      return false;
  }

}


template <class Basic> bool prepare(Basic value, AppOutput &ao, SeqToDo const toDo) {

  switch (auto const wrap = get<0>(ao).wrap(); wrap) {

    case EWrap::ArithOnly: {
      using WrapT = ArithOnly<Basic>;
      return prepare(value, WrapT(value), ao, toDo);
    }

    case EWrap::ArithWithMemFromHeap: {
      using WrapT = ArithWithMemFromHeap<Basic>;
      return prepare(value, WrapT(value), ao, toDo);
    }

    case EWrap::ArithWithStdVec: {
      using WrapT = ArithWithStdVec<Basic>;
      return prepare(value, WrapT(value), ao, toDo);
    }

    case EWrap::None: {
      using WrapT = Basic;
      return prepare(value, WrapT(value), ao, toDo);
    }

    default:
      return false;
  }

}


bool prepare(BasicType, AppOutput &ao, SeqToDo const toDo) {
  bool const verbo = get<0>(ao).isVerbose();

  auto const basicTypeOpt = get<0>(ao).basicType();
  if(!basicTypeOpt) {
    if(verbo) {
      get<1>(ao) << String("error: basictype has not been set, bailing\n");
    }
    return false;
  }

  switch (auto const value = *basicTypeOpt; value) {

    case bsChar:
      return prepare(char{}, ao, toDo);

    case bsSChar:
      return prepare(::SChar{}, ao, toDo);

    case bsUChar:
      return prepare(::UChar{}, ao, toDo);

    case bsWChar_t:
      return prepare(wchar_t{}, ao, toDo);

    case bsChar16_t:
      return prepare(char16_t{}, ao, toDo);

    case bsChar32_t:
      return prepare(char32_t{}, ao, toDo);

    case bsSShort:
      return prepare(::SShort{}, ao, toDo);

    case bsUShort:
      return prepare(::UShort{}, ao, toDo);

    case bsSInt:
      return prepare(::SInt{}, ao, toDo);

    case bsUInt:
      return prepare(::UInt{}, ao, toDo);

    case bsSLong:
      return prepare(::SLong{}, ao, toDo);

    case bsULong:
      return prepare(::ULong{}, ao, toDo);

    case bsSLongLong:
      return prepare(::SLongLong{}, ao, toDo);

    case bsULongLong:
      return prepare(::ULongLong{}, ao, toDo);

    default:
      return false;
  }

}



bool main_internal(int argc, char *argv[]) {
  bool res = false;
  auto out = ostringstream();
  auto errors = Errors();
  auto app = App();
  auto ao = AppOutput(app, out);
  parse(argc, argv, ao, errors);
  dealParamsUnprefixedOrUnknown(errors, ao);

  auto const toDo = get<0>(ao).toDo();
  bool const mayExit = get<0>(ao).mayExit();

  if (toDo != EToDo::Nothing && !mayExit) {
    switch (auto const what = toDo; what) {

      case EToDo::Nothing:
        break;

      case EToDo::DealWithIncorrectParams:
        dealParamsIncorrect(ao);
        break;

      case EToDo::FillAndEmptySeq:
        if(!mayExit){
          prepare(BasicType{}, ao, SeqToDo::FillAndEmptySeq);
          res = true;
        }
        break;

      case EToDo::GenInsertSeq:
        if(!mayExit){
          prepare(BasicType{}, ao, SeqToDo::GenerateInsertSeq);
          res = true;
        }
        break;
    }
  }

  std::cout << out.str();
  return res;
}


int main(int argc, char *argv[]) {
  int rc = EXIT_FAILURE;

  try {
    if (main_internal(argc, argv)) {
      rc = EXIT_SUCCESS;
    }
  } catch (std::exception const &ex) {
    cerr << "Caught a std::exception '" << ex.what() << "', bailing\n";
  } catch (...) {
    cerr << "Caught an unknown or unexpected exception type, bailing!\n";
  }

  return rc;
}
