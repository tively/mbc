# mbc

This project allows microbenchmarking C++ containers.

To build with MSVC (from console) : /std:c++latest /permissive- /W4 /EHsc /DUSE_MOVE (/bigobj may also be needed)

To build with gcc 13.2 or clang 17.0 : -Wall -Wextra -Wpedantic -Wshadow -Wconversion --std=c++2b -DUSE_MOVE
